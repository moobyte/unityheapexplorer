## Introduction
Heap Explorer is a memory profiler, debugger and analyzer for [Unity](http://unity3d.com).

I tested Heap Explorer with Unity 2017.4 LTS. Earlier versions are not supported on purpose. Capturing a memory snapshot with Unity 2017.4 and Scripting Runtime .NET 4.x does not work, see [here](https://forum.unity.com/threads/wip-heap-explorer-memory-profiler-debugger-and-analyzer-for-unity.527949/page-2#post-3679282) for a workaround.

## Video

[https://www.youtube.com/watch?v=tcTl_7y8JBA](https://www.youtube.com/watch?v=tcTl_7y8JBA)

## Forum discussion

[https://forum.unity.com/threads/wip-heap-explorer-memory-profiler-debugger-and-analyzer-for-unity.527949/](https://forum.unity.com/threads/wip-heap-explorer-memory-profiler-debugger-and-analyzer-for-unity.527949/)

## Documentation
[http://www.console-dev.de/bin/HeapExplorer.pdf](http://www.console-dev.de/bin/HeapExplorer.pdf)
